import Webpack from 'webpack';

export default {
    externals: {
        html10n: '_',
    },
    entry: './www/templates/vue/poll/Main.js',
    output: {
        path: __dirname + '/www/js-bin',
        filename: 'poll.js',
    },
    module: {
        loaders: [
            /*
             'style-loader' creates style nodes from JS strings
             'css-loader' translates CSS into CommonJS
             'less-loader' compiles Less to CSS
             */
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader',
            },
            {
                test: /\.css$/,
                loaders: ['style-loader', 'css-loader'],
            },
            /*{
                test: /\.(png|jpg|gif|svg)$/,
                loaders: ['url-loader', 'file-loader'],
            },*/
        ],
    },
    resolve: {
        alias: {
            'vue$': 'vue/dist/vue.esm.js',
        },
    },
    devtool: 'eval-source-map', // replace to 'source-map' on production
    plugins: [
        new Webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery"
        }),
        /*new Webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('production')
            }
        }),*/
        new Webpack.optimize.UglifyJsPlugin({minimize: true}),
    ],
};
